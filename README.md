# UniCon – Konverter jedinica #

## Opis aplikacije ##

UniCon je aplikacija koja preračunava mjerne jedinice za brzine prijenosa podataka (bit/sekundi, kilobajt/sekundi,...), prefikse (centi, mili, mikro,...), brzinu (metar/sekundi, milja/satu,...) i jedinice za pohranu podataka (bit, bajt, kilobajt, kibibajt, ...). Pokretanjem aplikacije prikazuju se četiri gumba na kojima je odgovarajuća slika koja opisuje o kojoj mjernoj jedinici se radi i tekst ako iz slike nije dovoljno jasno o kojoj se mjernoj jedinici radi. Klikom na gumb, otvara se novi activity u kojemu se iz padajućeg izbornika odabire iz koje i u koju mjernu jedinicu će se preračunavati, Edit Text u koji se mogu unijeti samo pozitivni brojevi te gumb koji na klik otvara novi activity i prikazuje rješenje preračunavanja mjernih jedinica.

## Rješenja i problemi pri stvaranju aplikacije ##

Za početni activity korišten je linearni layout gdje je svaki gumb jedan ispod drugog. Za slike pozadine svakog gumba korištene su slike s [1], [2], [3], [4] i Gimp za uređivanje slike (dodavanje pozadinske boje i prilagođavanje ikona). Eksplicitnim intentom, pritisak na svaki gumb odvede do svog activity-a. Na tim activity-ma dodane su padajuće liste koje su napravljene koristeći spinner, a odgovarajuća sintaksa i postavke u xml i java datoteci nađene su na linku pod [5]. Dodan je i Edit Text na kojemu je inputType postavljen na pozitivni decimalni broj. Još su ubačeni Text View-i koji objašnjavaju o kojoj se mjernoj jedinici radi i što se odabire u padajućem izborniku. Klikom na gumb koji se nalazi ispod svih objekata započinje preračunavanje mjernih jedinica. Iz spinner-a se dohvaća pozicija item-a kojeg je korisnik odabrao te se na temelju toga zna o kojoj mjernoj jedinici se radi pa je prema tome zadano određeno preračunavanje. Odabir mjernih jedini, unešenog broja i rješenja preračunavanja se sprema u objek klase DataTransfer koji služi za prijenos svih tih podataka između activity-a. Za prijenos, klasa DataTransfer implementira Parcelable interfejs uz pomoć linkova [6] i [7]. Koristeći extra podatka poslan je objekt klase DataTransfer u novi activity koji prikazuje rezultate koje je dobio iz prijašnjeg activity-a. Aplikacija može raditi u vertikalnoj i horizontalnoj orijentaciji zaslona. Za prikaz svih elemenata u horizontalnoj orijentaciji, svakom layout-u je dodana opcija scrollable koja je nađena preko linka [8].

Pri izradi aplikacije nije bilo velikih problema, no trebalo je dosta vremena pri pronalasku rješenja za svaku "prepreku" kao što su spinner, prenošenje cijelog objekta između activity-a, preračunavanje mjernih jedinica (linkovi [9], [10], [11] i [12] su poslužili za pravilno preračunavanje i odnose između mjernih jedinica), pronalazak ikona i odgovarajućih boja aplikacija. 
Svojom greškom, prvobitno su ubačene slike u JPEG formatu čime su se pojavili problemi "cannot resolve symbol R" i odgovor na taj problem je nađen na linku [13] te sve slike pretvorene u PNG format.

## Testiranje ##

Slike aplikacije u radu se nalaze u mapi "Slike aplikacije u radu", gdje su slike test1-X screenshot-ovi aplikacije na mobitelu zaslona 5'' HD i 4.4.2 verziji Androida, a test2-X screenshot-ovi na mobitelu zaslona 5.5'' FHD i 5.1 verziji Androida.

### Linkovi na vanjske resurse ###

[1]https://www.google.hr/search?client=firefox-b&biw=1536&bih=770&tbm=isch&sa=1&q=data+transfer+icon+&oq=data+transfer+icon+&gs_l=img.3..0i19k1l2j0i30i19k1.59431.59431.2.59651.1.0.1.0.0.0.0.0..0.0....0...1c.1.64.img..0.1.5.rFEjIX-5ZbQ#imgrc=zm1lYlNceTSAZM:

[2]https://www.google.hr/search?client=firefox-b&biw=1536&bih=770&tbm=isch&sa=1&q=p++icon&oq=p++icon&gs_l=img.3..0i19k1l9j0i5i30i19k1.725971.725971.0.726237.1.1.0.0.0.0.256.256.2-1.1.0....0...1c.1.64.img..0.1.255.4Ox6v_w3GKA#imgrc=jRJPjyZKrQFI1M:

[3]https://www.google.hr/search?client=firefox-b&biw=1536&bih=770&tbm=isch&sa=1&q=speed+icon+grey&oq=speed+icon+grey&gs_l=img.3...2629.4335.0.4537.5.5.0.0.0.0.213.837.0j3j2.5.0....0...1c.1.64.img..0.2.392...0i19k1j0i30i19k1j0i5i30i19k1j0i8i30k1.1sTpyA9RAoE#imgdii=u6oiIplEbkNvjM:&imgrc=xDa2rfHoki4QXM:

[4]https://www.google.hr/search?client=firefox-b&biw=1536&bih=770&tbm=isch&sa=1&q=data+storage+icon+&oq=data+storage+icon+&gs_l=img.3..0i19k1l2j0i30i19k1.3857228.3857850.0.3858205.4.4.0.0.0.0.161.508.0j4.4.0....0...1c.1.64.img..0.1.122.9P2CvBcaok8#imgrc=L8bs6Sf2NlvTgM:

[5]https://www.mkyong.com/android/android-spinner-drop-down-list-example/

[6]http://sohailaziz05.blogspot.hr/2012/04/passing-custom-objects-between-android.html

[7]http://stackoverflow.com/questions/2736389/how-to-pass-an-object-from-one-activity-to-another-on-android

[8]http://stackoverflow.com/questions/4055537/how-do-you-make-a-linearlayout-scrollable

[9]http://www.unitconverters.net/data-transfer-converter.html

[10]http://www.unitconverters.net/prefixes-converter.html

[11]http://www.unitconverters.net/speed-converter.html

[12]http://www.thecalculatorsite.com/conversions/datastorage.php

[13]http://stackoverflow.com/questions/17054000/cannot-resolve-symbol-r-in-android-studio