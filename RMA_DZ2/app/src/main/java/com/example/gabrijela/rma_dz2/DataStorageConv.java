package com.example.gabrijela.rma_dz2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class DataStorageConv extends AppCompatActivity implements View.OnClickListener {

    Spinner spFrom, spTo;
    Button bDataStorageResult;
    EditText etData;
    TransferClass obj=new TransferClass();
    float temp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_storage_conv);
        this.spFrom = (Spinner) findViewById(R.id.spDataStorageFrom);

        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.DataStorageUnits,
                        android.R.layout.simple_spinner_item);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFrom.setAdapter(staticAdapter);
        this.spTo = (Spinner) findViewById(R.id.spDataStorageTo);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTo.setAdapter(staticAdapter);
        this.setUpUI();
    }

    private void setUpUI() {
        this.bDataStorageResult= (Button)findViewById(R.id.bResult);
        this.etData= (EditText) findViewById(R.id.etDataStorageData);
        bDataStorageResult.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(),Result.class);
        int unit1 =spFrom.getSelectedItemPosition();
        int unit2=spTo.getSelectedItemPosition();
        float data;
        if(etData.getText().toString().matches("")){
            data=0;
            obj.setData(data);
        }
        else{
            data=Float.parseFloat(etData.getText().toString());
            obj.setData(data);
        }
        obj.setUnit1(spFrom.getSelectedItem().toString());
        obj.setUnit2(spTo.getSelectedItem().toString());
        float result;
        //b
        if(unit1==0){
            if(unit2==0){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 0.125f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 125e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=(1/8192f)*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=125*1e-9f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                temp=1024*1024*8;
                result=(1/temp)*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=125*1e-12f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                temp=1024*1024;
                temp=temp*1024;
                temp=temp*8;
                result=(1/temp)*data;
                obj.setResult(result);
            }
        }
        //B
        if(unit1==1){
            if(unit2==0){
                result=8*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 0.001f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=(1/1024f)*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=1e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                temp=1024*1024;
                result=(1/temp)*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1e-9f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                temp=1024*1024;
                temp=temp*1024*8;
                result=(1/temp)*data;
                obj.setResult(result);
            }
        }
        //kB
        if(unit1==2){
            if(unit2==0){
                result=8000f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1000f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=(1000/1024f)*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=1e-3f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                temp=1024*1024;
                result=(1000/temp)*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                temp=1024*1024;
                temp=temp*1024;
                result=(1000/temp)*data;
                obj.setResult(result);
            }
        }
        //KiB
        if(unit1==3){
            if(unit2==0){
                result=8192f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1024f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=1.024f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=0.001024f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=(1/1024f)*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1024*1e-9f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                temp=1024*1024;
                result=(1/temp)*data;
                obj.setResult(result);
            }
        }
        //MB
        if(unit1==4){
            if(unit2==0){
                result=8e6f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1e6f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 1000f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=(1e6f/1024f)*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==5){
                temp=1024*1024;
                result=(1e6f/temp)*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1e-3f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                temp=1024*1024;
                temp=temp*1024;
                result=(1/temp)*data;
                obj.setResult(result);
            }
        }
        //MiB
        if(unit1==5){
            if(unit2==0){
                result=8388608f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1048576f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=1048.576f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=1024f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=1.048576f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=0.001048576f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=0.0009765625f*data;
                obj.setResult(result);
            }
        }
        //GB
        if(unit1==6){
            if(unit2==0){
                result=8e9f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1e9f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=1e6f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=976562.5f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=1e3f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=953.67431640625f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=0.93132257461548f*data;
                obj.setResult(result);
            }
        }
        //GiB
        if(unit1==7){
            if(unit2==0){
                result=8589934592f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1073741824f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=1073741.824f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=1048576f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=1073.741824f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=1024f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1.073741824f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=data;
                obj.setResult(result);
            }
        }
        intent.putExtra("myCustomerObj",obj);
        this.startActivity(intent);
    }
}
