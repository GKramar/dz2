package com.example.gabrijela.rma_dz2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class DataTransferConv extends AppCompatActivity implements View.OnClickListener{
    Spinner spFrom, spTo;
    Button bDataTransResult;
    EditText etData;
    TransferClass obj=new TransferClass();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_transfer_conv);
        this.spFrom = (Spinner) findViewById(R.id.spDataTransFrom);

        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.DataTransUnits,
                        android.R.layout.simple_spinner_item);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFrom.setAdapter(staticAdapter);
        this.spTo = (Spinner) findViewById(R.id.spDataTransTo);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTo.setAdapter(staticAdapter);
        this.setUpUI();
    }

    private void setUpUI() {
        this.bDataTransResult= (Button)findViewById(R.id.bResult);
        this.etData= (EditText) findViewById(R.id.etTransData);
        bDataTransResult.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(),Result.class);
        int unit1 =spFrom.getSelectedItemPosition();
        int unit2=spTo.getSelectedItemPosition();
        float data;
        if(etData.getText().toString().matches("")){
            data=0;
            obj.setData(data);
        }
        else{
            data=Float.parseFloat(etData.getText().toString());
            obj.setData(data);
        }
        obj.setUnit1(spFrom.getSelectedItem().toString());
        obj.setUnit2(spTo.getSelectedItem().toString());
        float result;
        //b/s
        if(unit1==0){
            if(unit2==0){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= (0.125f*data);
                obj.setResult(result);
            }
            else if(unit2==2){
                result= (1/1000f)*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=(1/8000f)*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=1e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=1e-6f*0.125f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1e-9f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-9f*0.125f*data;
                obj.setResult(result);
            }
        }
        //B/s
        if(unit1==1){
            if(unit2==0){
                result=8*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 0.008f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=1e-3f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=8e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=1e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=8e-9f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-9f*data;
                obj.setResult(result);
            }
        }
        //kb/s
        if(unit1==2){
            if(unit2==0){
                result=1000f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result=125f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=0.125f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=0.001f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=1e-3f*0.125f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-6f*0.125f*data;
                obj.setResult(result);
            }
        }
        //kB/s
        if(unit1==3){
            if(unit2==0){
                result=8000f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result=1000f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=8f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=8e-3f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=1e-3f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=8e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-6f*data;
                obj.setResult(result);
            }
        }
        //Mb/s
        if(unit1==4){
            if(unit2==0){
                result=1e6f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result=125e3f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=1e3f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=125f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=0.125f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1e-3f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-3f*0.125f*data;
                obj.setResult(result);
            }
        }
        //MB/s
        if(unit1==5){
            if(unit2==0){
                result=8e6f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result=1e6f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=8e3f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=1e3f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=8*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=8e-3f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-3f*data;
                obj.setResult(result);
            }
        }
        //Gb/s
        if(unit1==6){
            if(unit2==0){
                result=1e9f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result=125e6f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=1e6f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=125e3f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=1e3f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=125f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=0.125f*data;
                obj.setResult(result);
            }
        }
        //GB/s
        if(unit1==7){
            if(unit2==0){
                result=8e9f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result=1e9f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=8e6f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=1e6f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=8e3f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=1e3f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=8*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=data;
                obj.setResult(result);
            }
        }
        intent.putExtra("myCustomerObj",obj);
        this.startActivity(intent);
    }
}
