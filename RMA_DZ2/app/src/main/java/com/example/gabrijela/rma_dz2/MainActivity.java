package com.example.gabrijela.rma_dz2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button bDataTransfer,bPrefix,bSpeed,bDataStorage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpUI();
    }
    private void setUpUI() {
        this.bDataTransfer= (Button) findViewById(R.id.bDataTransfer);
        this.bPrefix= (Button) findViewById(R.id.bPrefix);
        this.bSpeed= (Button) findViewById(R.id.bSpeed);
        this.bDataStorage= (Button) findViewById(R.id.bDataStorage);
        bDataTransfer.setOnClickListener(this);
        bPrefix.setOnClickListener(this);
        bSpeed.setOnClickListener(this);
        bDataStorage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent explicitIntent = null;
        switch (v.getId()){
            case(R.id.bDataTransfer):
                explicitIntent=new Intent();
                explicitIntent.setClass(getApplicationContext(), DataTransferConv.class);
                break;
            case(R.id.bPrefix):
                explicitIntent=new Intent();
                explicitIntent.setClass(getApplicationContext(), PrefixConv.class);
                break;
            case(R.id.bSpeed):
                explicitIntent=new Intent();
                explicitIntent.setClass(getApplicationContext(), SpeedConv.class);
                break;
            case(R.id.bDataStorage):
                explicitIntent=new Intent();
                explicitIntent.setClass(getApplicationContext(), DataStorageConv.class);
                break;
        }

        this.startActivity(explicitIntent);
    }
}
