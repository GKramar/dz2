package com.example.gabrijela.rma_dz2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class PrefixConv extends AppCompatActivity implements View.OnClickListener{
    Spinner spFrom, spTo;
    Button bPrefixResult;
    EditText etData;
    TransferClass obj=new TransferClass();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prefix_conv);
        this.spFrom = (Spinner) findViewById(R.id.spPrefixFrom);

        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.PrefixUnits,
                        android.R.layout.simple_spinner_item);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFrom.setAdapter(staticAdapter);
        this.spTo = (Spinner) findViewById(R.id.spPrefixTo);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTo.setAdapter(staticAdapter);
        this.setUpUI();
    }

    private void setUpUI() {
        this.bPrefixResult= (Button)findViewById(R.id.bResult);
        this.etData= (EditText) findViewById(R.id.etPrefixData);
        bPrefixResult.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(),Result.class);
        int unit1 =spFrom.getSelectedItemPosition();
        int unit2=spTo.getSelectedItemPosition();
        float data;
        if(etData.getText().toString().matches("")){
            data=0;
            obj.setData(data);
        }
        else{
            data=Float.parseFloat(etData.getText().toString());
            obj.setData(data);
        }
        obj.setUnit1(spFrom.getSelectedItem().toString());
        obj.setUnit2(spTo.getSelectedItem().toString());
        float result;
        //none
        if(unit1==0){
            if(unit2==0){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1e6f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 1e3f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=1e2f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=10f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=0.1f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=0.001f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-6f*data;
                obj.setResult(result);
            }
        }
        //micro
        if(unit1==1){
            if(unit2==0){
                result=1e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 0.001f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=0.0001f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=0.00001f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=1e-7f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1e-9f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-12f*data;
                obj.setResult(result);
            }
        }
        //mili
        if(unit1==2){
            if(unit2==0){
                result=0.001f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1000f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=0.1f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=0.01f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=0.0001f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-9f*data;
                obj.setResult(result);
            }
        }
        //centi
        if(unit1==3){
            if(unit2==0){
                result=0.01f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 10000f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 10f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=0.1f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=0.001f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1e-6f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-8f*data;
                obj.setResult(result);
            }
        }
        //deci
        if(unit1==4){
            if(unit2==0){
                result=0.1f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result=1e5f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=100f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=10f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=0.01f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=0.0001f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-7f*data;
                obj.setResult(result);
            }
        }
        //deka
        if(unit1==5){
            if(unit2==0){
                result=10f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1e7f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 1e4f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=1000f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=100f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=0.01f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e-5f*data;
                obj.setResult(result);
            }
        }
        //kilo
        if(unit1==6){
            if(unit2==0){
                result=1000f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1e9f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=1e6f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=1e5f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=1e4f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=100f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=0.001f*data;
                obj.setResult(result);
            }
        }
        //mega
        if(unit1==7){
            if(unit2==0){
                result=1e6f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1e12f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=1e9f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=1e8f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=1e7f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=1e5f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1000f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=data;
                obj.setResult(result);
            }
        }
        intent.putExtra("myCustomerObj",obj);
        this.startActivity(intent);
    }
}
