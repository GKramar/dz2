package com.example.gabrijela.rma_dz2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Result extends AppCompatActivity {

    TextView tvUnit1,tvUnit2,tvData,tvResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        tvUnit1= (TextView) findViewById(R.id.tvFromUnit);
        tvUnit2= (TextView) findViewById(R.id.tvToUnit);
        tvData= (TextView) findViewById(R.id.tvFromData);
        tvResult= (TextView) findViewById(R.id.tvToData);
        setUpUI();
    }
    private void setUpUI() {
        Intent i = getIntent();
        TransferClass obj = i.getParcelableExtra("myCustomerObj");
        tvUnit1.setText(obj.getUnit1());
        tvUnit2.setText(obj.getUnit2());
        tvData.setText(String.valueOf(obj.getData()));
        tvResult.setText(String.valueOf(obj.getResult()));
    }
}
