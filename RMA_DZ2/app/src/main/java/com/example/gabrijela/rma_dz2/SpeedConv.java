package com.example.gabrijela.rma_dz2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class SpeedConv extends AppCompatActivity implements View.OnClickListener{
    Spinner spFrom, spTo;
    Button bSpeedResult;
    EditText etData;
    TransferClass obj=new TransferClass();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed_conv);
        this.spFrom = (Spinner) findViewById(R.id.spSpeedFrom);

        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.SpeedUnits,
                        android.R.layout.simple_spinner_item);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFrom.setAdapter(staticAdapter);
        this.spTo = (Spinner) findViewById(R.id.spSpeedTo);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTo.setAdapter(staticAdapter);
        this.setUpUI();
    }

    private void setUpUI() {
        this.bSpeedResult= (Button)findViewById(R.id.bResult);
        this.etData= (EditText) findViewById(R.id.etSpeedData);
        bSpeedResult.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(),Result.class);
        int unit1 =spFrom.getSelectedItemPosition();
        int unit2=spTo.getSelectedItemPosition();
        float data;
        if(etData.getText().toString().matches("")){
            data=0;
            obj.setData(data);
        }
        else{
            data=Float.parseFloat(etData.getText().toString());
            obj.setData(data);
        }
        obj.setUnit1(spFrom.getSelectedItem().toString());
        obj.setUnit2(spTo.getSelectedItem().toString());
        float result;
        //m/s
        if(unit1==0){
            if(unit2==0){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 3.6f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 2.2369362921f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=60f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=0.06f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=0.001f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=360000f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=100f*data;
                obj.setResult(result);
            }
        }
        //km/h
        if(unit1==1){
            if(unit2==0){
                result=0.27777777777f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= (1000/3600f)*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 0.6213711922f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=(600/36f)*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=(1/60f)*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=(1/3600f)*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=1e5f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=27.7777777f*data;
                obj.setResult(result);
            }
        }
        //mi/s
        if(unit1==2){
            if(unit2==0){
                result=0.44704f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1.609344f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=26.8224f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=0.0268224f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=0.00044704f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=160934.4f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=44.704f*data;
                obj.setResult(result);
            }
        }
        //m/min
        if(unit1==3){
            if(unit2==0){
                result=0.0166666666f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 0.06f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 0.0372822715f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=0.001f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=0.000016666f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=6000f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1.666666666f*data;
                obj.setResult(result);
            }
        }
        //km/min
        if(unit1==4){
            if(unit2==0){
                result=16.666666666f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 60f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 37.282271534f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=1000f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=0.0166666666f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=6e6f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1666.66666666f*data;
                obj.setResult(result);
            }
        }
        //km/s
        if(unit1==5){
            if(unit2==0){
                result=1000f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 3600f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 2236.9362921f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=6e4f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=60f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=36e7f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=1e5f*data;
                obj.setResult(result);
            }
        }
        //cm/h
        if(unit1==6){
            if(unit2==0){
                result=2777e-9f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 1e-5f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 0.0000062137f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=0.00016666666f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=1.66666666666f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=2.777777777777e-9f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=0.000277777f*data;
                obj.setResult(result);
            }
        }
        //cm/s
        if(unit1==7){
            if(unit2==0){
                result=0.01f*data;
                obj.setResult(result);
            }
            else if(unit2==1){
                result= 0.036f*data;
                obj.setResult(result);
            }
            else if(unit2==2){
                result= 0.0223693629f*data;
                obj.setResult(result);
            }
            else if(unit2==3){
                result=0.6f*data;
                obj.setResult(result);
            }
            else if(unit2==4){
                result=0.0006f*data;
                obj.setResult(result);
            }
            else if(unit2==5){
                result=0.00001f*data;
                obj.setResult(result);
            }
            else if(unit2==6){
                result=3600f*data;
                obj.setResult(result);
            }
            else if(unit2==7){
                result=data;
                obj.setResult(result);
            }
        }
        intent.putExtra("myCustomerObj",obj);
        this.startActivity(intent);
    }
}
