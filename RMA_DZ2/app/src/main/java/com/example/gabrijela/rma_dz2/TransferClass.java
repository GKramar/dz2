package com.example.gabrijela.rma_dz2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Gabrijela on 5.4.2017..
 */

class TransferClass implements Parcelable{

        private String unit1, unit2;
        float data, result;

        public TransferClass(String unit1, String unit2, float data, float result) {
            this.unit1 = unit1;
            this.unit2 = unit2;
            this.data = data;
            this.result = result;
        }
    public TransferClass() {
        this.unit1 = "";
        this.unit2 = "";
        this.data = 0;
        this.result = 0;
    }


        public String getUnit1() {
            return unit1;
        }

        public void setUnit1(String unit1) {
            this.unit1 = unit1;
        }

        public String getUnit2() {
            return unit2;
        }

        public void setUnit2(String unit2) {
            this.unit2 = unit2;
        }

        public float getData() {
            return data;
        }

        public void setData(float data) {
            this.data = data;
        }

        public float getResult() {
            return result;
        }

        public void setResult(float result) {
            this.result = result;
        }

        public TransferClass(Parcel in) {
            readFromParcel(in);
        }

        public static final Parcelable.Creator<TransferClass> CREATOR = new Parcelable.Creator<TransferClass>() {
            public TransferClass createFromParcel(Parcel in) {
                return new TransferClass(in);
            }

            public TransferClass[] newArray(int size) {
                return new TransferClass[size];
            }
        };


        @Override
        public int describeContents() {
            return 0;
        }


        @Override
        public void writeToParcel(Parcel dest, int flags) {

            dest.writeString(unit1);
            dest.writeString(unit2);
            dest.writeFloat(data);
            dest.writeFloat(result);
        }

        private void readFromParcel(Parcel in) {

            unit1 = in.readString();
            unit2 = in.readString();
            data = in.readFloat();
            result = in.readFloat();
        }
}


